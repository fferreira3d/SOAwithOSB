<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[(:: pragma bea:global-element-parameter parameter="$resultadoSerasa" element="ns0:ResultadoSerasa" location="../Interfaces/Schemas/Serasa.xsd" ::)
(:: pragma bea:global-element-return element="ns2:resultadoVerificaCreditoExterno" location="../../../../EBS/Credito/xsd/ParametrosCredito.xsd" ::)

declare namespace ns2 = "http://pucminas.br/SN/Credito";
declare namespace ns1 = "http://pucminas.br/MC/SOA/Biharck";
declare namespace ns0 = "http://serasa.com.br/SN/Serasa";
declare namespace xf = "http://tempuri.org/OSB_FelipeFerreira/ServicosNegocio/Credito/Resources/Transformations/ResultadoSerasaToResultadoVerificaCreditoExterno/";

declare function xf:ResultadoSerasaToResultadoVerificaCreditoExterno($resultadoSerasa as element(ns0:ResultadoSerasa))
    as element(ns2:resultadoVerificaCreditoExterno) {
        <ns2:resultadoVerificaCreditoExterno>
            <ns2:credito>
                <ns1:pessoa>
                    <ns1:cnpj>{ data($resultadoSerasa/ns0:num_documento) }</ns1:cnpj>
                </ns1:pessoa>
                <ns1:indicaCreditoExterno>
                    {
                        if (xs:long(data($resultadoSerasa/ns0:valor_restricao)) > 0) then
                            (fn:true())
                        else 
                            (fn:false())
                    }
				</ns1:indicaCreditoExterno>
				<ns1:situacao>
					{
                        if (xs:integer($resultadoSerasa/ns0:valor_restricao) > 400) then
                            (200)
                        else 
                            (0)
                    }
				</ns1:situacao>
            </ns2:credito>
        </ns2:resultadoVerificaCreditoExterno>
};

declare variable $resultadoSerasa as element(ns0:ResultadoSerasa) external;

xf:ResultadoSerasaToResultadoVerificaCreditoExterno($resultadoSerasa)]]></con:xquery>
    <con:dependency location="../Interfaces/Schemas/Serasa.xsd">
        <con:schema ref="OSB_FelipeFerreira/ServicosNegocio/Credito/Resources/Interfaces/Schemas/Serasa"/>
    </con:dependency>
    <con:dependency location="../../../../EBS/Credito/xsd/ParametrosCredito.xsd">
        <con:schema ref="OSB_FelipeFerreira/EBS/Credito/xsd/ParametrosCredito"/>
    </con:dependency>
</con:xqueryEntry>