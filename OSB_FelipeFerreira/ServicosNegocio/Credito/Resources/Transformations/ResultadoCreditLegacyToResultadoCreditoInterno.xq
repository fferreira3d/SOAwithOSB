(:: pragma bea:global-element-parameter parameter="$resultadoCreditLegacy" element="ns0:ResultadoCreditLegacy" location="../Interfaces/Schemas/CreditLegacy.xsd" ::)
(:: pragma bea:global-element-return element="ns2:resultadoVerificaCreditoInterno" location="../../../../EBS/Credito/xsd/ParametrosCredito.xsd" ::)

declare namespace ns2 = "http://pucminas.br/SN/Credito";
declare namespace ns1 = "http://pucminas.br/MC/SOA/Biharck";
declare namespace ns0 = "http://creditlegacy.com.br/SN/CreditLegacy";
declare namespace xf = "http://tempuri.org/OSB_FelipeFerreira/ServicosNegocio/Credito/Resources/Transformations/CreditLegacyResponseToCreditoInternoResponse/";

declare function xf:CreditLegacyResponseToCreditoInternoResponse($resultadoCreditLegacy as element(ns0:ResultadoCreditLegacy))
    as element(ns2:resultadoVerificaCreditoInterno) {
        <ns2:resultadoVerificaCreditoInterno>
            <ns2:credito>
                <pessoa>
                    <cnpj>{ data($resultadoCreditLegacy/ns0:cpf) }</cnpj>
                </pessoa>
                <situacao>
                    {
                        if ((fn:count($resultadoCreditLegacy/ns0:restricoes/ns0:restricao/ns0:valor) <= 3 and fn:sum($resultadoCreditLegacy/ns0:restricoes/ns0:restricao/ns0:valor) < 200)) then
                            (500)
                        else 
                            if (fn:count($resultadoCreditLegacy/ns0:restricoes/ns0:restricao/ns0:valor) >= 4) then
                                (0)
                            else 
                                (900)
                    }
</situacao>
            </ns2:credito>
        </ns2:resultadoVerificaCreditoInterno>
};

declare variable $resultadoCreditLegacy as element(ns0:ResultadoCreditLegacy) external;

xf:CreditLegacyResponseToCreditoInternoResponse($resultadoCreditLegacy)
